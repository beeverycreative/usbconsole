package usbconsole;

/**
 *
 * @author mdomingues
 */
public class Consumer extends Thread {

    private USBConsole console;
    private NewJFrame frame;
    private Evaluater ev;

    public Consumer(USBConsole console, NewJFrame frame, Evaluater ev) {
        this.console = console;
        this.frame = frame;
        this.ev = ev;
    }

    @Override
    public void run() {

        while (true) {

            String command = null;
//             if (!ev.okToGo()) {
                //Thread.sleep(50);
                command = console.readResponse();
//            } 
            if(!(command==null)) {
                System.out.println("Chegou ="+command);
                ev.setCommand_received(command);
                //frame.updateOutput(command);
            }
        }
    }
}
