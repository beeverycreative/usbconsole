package usbconsole;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdomingues
 */
public class Producer extends Thread {

    private USBConsole console;
    private String[] commands;
    private NewJFrame frame;
    private Evaluater ev;

    public Producer(USBConsole console, NewJFrame frame, Evaluater ev) {
        this.commands = new String[]{"G28 xy",
            "M300",
            "G1 F2000",
            "M104 S220",
            "M300",
            "M605 z"};
        this.console = console;
        this.frame = frame;
        this.ev = ev;
    }

    @Override
    public void run() {

        int i = 0;
        long start = System.currentTimeMillis();
        long now = System.currentTimeMillis();
        String command;
        while (true) {

//            if (ev.okToGo()) {
//            long now = System.currentTimeMillis() + 1;
//
//            //System.out.println("Entrou =" + i);
//            while (System.currentTimeMillis() < now) {
//            };

            System.out.println(String.valueOf(i).concat(String.format("%0" + (63 - String.valueOf(i).length()) + "d", 0)));
            console.sendCommand(String.valueOf(i).concat(String.format("%0" + (63 - String.valueOf(i).length()) + "d", 0)));
//            command = console.readResponse();
//            ev.setCommand_Sent(String.valueOf(i).concat(String.format("%0" + (63 - String.valueOf(i).length()) + "d", 0)));
            i++;
//            if (i % 1000 == 0)
//            {now = System.currentTimeMillis();
//                System.out.println("Tic "+ i+ " Time:"+(now-start));
//                start = System.currentTimeMillis();
        }
    }
//            
//        }
}
//            if (i == 10000) {
//                long now = System.currentTimeMillis();
//                System.out.println("passaram 10000 em " + (now - start));
//                i = 0;
//                start = now;

