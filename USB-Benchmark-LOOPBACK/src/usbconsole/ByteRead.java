/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package usbconsole;

/**
 *
 * @author rui
 */
public class ByteRead<X, Y> {

    public int size;
    public byte[] byte_array;

    public ByteRead(int x, byte[] y) {
        this.size = x;
        this.byte_array = y;
    }

    public ByteRead() {
        this.size = 0;
        this.byte_array = null;
    }
}
