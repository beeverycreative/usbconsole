package usbconsole;

/**
 *
 * @author mdomingues
 */
public class Main {

    private static USBConsole console = new USBConsole();
    private static NewJFrame frame = new NewJFrame(console);
    private static Evaluater ev = new Evaluater();

    public static void main(String[] args) {
        //Tester tester = new Tester(console, frame,ev);
        //Producer prod = new Producer(console, frame,ev);

        console.initialize();
        frame.setVisible(true);

        //prod.start();
        //tester.start();

    }
}
