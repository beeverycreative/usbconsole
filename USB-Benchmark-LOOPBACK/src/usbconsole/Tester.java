package usbconsole;

/**
 *
 * @author mdomingues
 */
public class Tester extends Thread {

    private USBConsole console;
    private NewJFrame frame;
    private Evaluater ev;

    private int messageSize = 64;
    
    private byte[] byteMessage = new byte[64];
    private byte current_byte = '0';
    private int message_number;
    private int total_bytes;
    private boolean started = false;
    private int ok_bytes;
    private int expected_bytes= 1024;
    private long start;
    private long total;
    private long loop;
    private long send;
    private long read;
    private String resultString = "";
    
    public Tester(USBConsole console, NewJFrame frame, Evaluater ev) {
        this.console = console;
        this.frame = frame;
        this.ev = ev;
    }

    @Override
    public void run() {       
        total = 0;
        while (true || total_bytes< expected_bytes) {

           // byteMessage = messageGenerator();
            ByteRead result = null;
            int tries = 5;

                loop = System.nanoTime();
                console.sendCommandBytes(byteMessage);
                send = System.nanoTime()- loop;
                read = 0;
                
                while (tries > 0 && resultString.toLowerCase().contains("ok")){

                    try {
                        loop = System.nanoTime();
                    resultString = console.readResponse();
                    read = System.nanoTime()- loop;
                    tries--;
                    
                    }catch(Exception en){
                    ;
                    }
                }
                resultString = "";
                total += (read + send);
//            } 

                
              // System.out.println("^:"+send +" v:"+read+" t:"+total+" m:"+message_number+" byte:"+result.size+" %:"+ok_bytes+"/"+result.size);
                
                //frame.updateOutput(command);
            
        }
    }



    private boolean message_ok(ByteRead result) {
        int count_ok=0;
        
        if (messageSize==result.size){
            for(int i = 0; i<messageSize;i++)
                if (byteMessage[i]==result.byte_array[i])
                    count_ok++;
            
            if (count_ok==messageSize){
                return true;
            }  
        }//No need for else
        return false;    
    }
}
