package usbconsole;

import de.ailis.usb4java.AbstractDevice;
import de.ailis.usb4java.libusb.Device;
import de.ailis.usb4java.libusb.DeviceHandle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.usb.UsbClaimException;
import javax.usb.UsbConfiguration;
import javax.usb.UsbConst;
import javax.usb.UsbDevice;
import javax.usb.UsbDeviceDescriptor;
import javax.usb.UsbDisconnectedException;
import javax.usb.UsbEndpoint;
import javax.usb.UsbException;
import javax.usb.UsbHostManager;
import javax.usb.UsbHub;
import javax.usb.UsbInterface;
import javax.usb.UsbNotActiveException;
import javax.usb.UsbNotOpenException;
import javax.usb.UsbServices;

/**
 *
 * @author bitbox
 */
public class USBConsole {

    private static final String RESPONSE_OK = "ok";
    protected static UsbDevice m_usbDevice;
    protected ArrayList<UsbDevice> m_usbDeviceList = new ArrayList<UsbDevice>();
    private String m_manufacturer = "BEEVERYCREATIVE";
    private String m_product = "BEETHEFIRST - ";
    private short VENDOR_ID = (short) 0xffff;
    private short productId = new Short((short) 0x014e);
    private short productId2 = new Short((short) 0x014e);
    private String firmware_manufacturer = "BEEVERYCREATIVE";
    private String firmware_product = "BEETHEFIRST - firmware";
    private String bootloader_manufacturer = "BEEVERYCREATIVE";
    private String bootloader_product = "BEETHEFIRST - bootloader";
    /**
     * Lock for multi-threaded access to this driver's serial port.
     */
    private final ReentrantReadWriteLock m_usbLock = new ReentrantReadWriteLock();
    /**
     * Locks the serial object as in use so that it cannot be disposed until it
     * is unlocked. Multiple threads can hold this lock concurrently.
     */
    public final ReentrantReadWriteLock.ReadLock m_usbInUse = m_usbLock.readLock();
    protected UsbPipes pipes;
    /**
     * What did we get back from serial?
     */
    private String result = "";
    private DecimalFormat df;
    private int countRead = 0;
    private boolean showMessage = true;
    private AtomicBoolean isInitialized = new AtomicBoolean(false);

    public void initialize() {

        // wait till we're initialized
        if (!isInitialized()) {
            // record our start time.
            Date date = new Date();
            //long end = date.getTime() + 10000;

            System.out.println("Initializing usb device.");
            while (!isInitialized()) {
                try {
                    InitUsbDevice();
                    if (m_usbDevice != null) {
                        pipes = GetPipe(m_usbDevice);

                        showMessage = true;
                    } else {
                        setInitialized(false);
                        if (showMessage) {
                            System.out.println("Error while initializing device. Device not present. ");
                        }
                    }
                    openPipe(pipes);
                } catch (Exception e) {
                    try {
                        Thread.sleep(500); // sleep 500 ms
                    } catch (InterruptedException ex) {
                        System.out.println("error while sleeping in open usb connection initialization: " + ex.getMessage());
                    }
                }

                // record our current time
                date = new Date();
                long now = date.getTime();


                if (showMessage) {
                    System.out.println("usb device non-responsive. Please plug in the USB device !");
                    //Stop showing the message 1 time is enough :) 
                    showMessage = false;
                }
            }

            try {
                /* if (pipes != null) {
                 closePipe(pipes);
                 }*/
                System.out.println("Ready.");
            } catch (Exception ex) {
                System.out.println("error while finalizing the usb connection initialization: " + ex.getMessage());
                // setInitialized(false);
                setInitialized(false);
                return;
            } finally {
                // Messages could come back later
                showMessage = true;
            }
        }
        //sendCommand("M300");
        // default us to absolute positioning
        //sendCommand("G28");
    }

    protected boolean pipesAvailable() {
        // Grab a lock
        m_usbLock.writeLock().lock();

        UsbDevice newDevice = null;
        try {
            UsbServices services = UsbHostManager.getUsbServices();
            UsbHub rootHub = services.getRootUsbHub();
            List devices = rootHub.getAttachedUsbDevices();

            // select correct device
            for (Object deviceObj : devices) {
                newDevice = (UsbDevice) deviceObj;

                if (newDevice.getManufacturerString().trim().contains(m_manufacturer)
                        && newDevice.getProductString().trim().contains(m_product)) {
                    System.out.println("Connecting to machine using usb device: " + newDevice.getProductString());
                    m_usbDevice = newDevice;
                    break;
                }
            }
        } catch (Exception e) {
            String message = "Error finding USB device: " + e.getMessage();
            System.out.println(message);
            return false;
            //setError(message);
        } finally {
            setInitialized(false);
            m_usbDevice = newDevice;
            m_usbLock.writeLock().unlock();
        }

        return true;
    }

    public UsbDevice findDevice(UsbHub hub, short vendorId, short productId) {
        for (UsbDevice device : (List<UsbDevice>) hub.getAttachedUsbDevices()) {
            UsbDeviceDescriptor desc;
            desc = device.getUsbDeviceDescriptor();

            if (desc.idVendor() == vendorId && (desc.idProduct() == productId
                    || desc.idProduct() == productId2)) {
                return device;
            }
            if (device.isUsbHub()) {
                device = findDevice((UsbHub) device, vendorId, productId);
                if (device != null) {
                    return device;
                }
            }
        }
        return null;
    }

    protected int sendCommand(String next) {

        next = clean(next);
        int cmdlen = 0;

        // skip empty commands.
        if (next.length() == 0) {
            return 0;
        }

        pipes = GetPipe(m_usbDevice);
        // do the actual send.
        String message = next + "\n";
        try {
            synchronized (m_usbDevice) {
                if (!pipes.isOpen()) {
                    openPipe(pipes);
                }

                pipes.getUsbPipeWrite().syncSubmit(message.getBytes());

                cmdlen = next.length() + 1;
            }
        } catch (Exception ex) {
            System.out.println("Error while sending command " + next + " : " + ex.getMessage());
            return 0;
        }
        return cmdlen;
    }

    /**
     * Send command to Machine
     *
     * @param next Command
     * @return command length
     */
    protected int sendCommandBytes(byte[] next) {

        int cmdlen = 0;
        int i = 0;

        pipes = GetPipe(m_usbDevice);

        try {
            synchronized (m_usbDevice) {
                if (!pipes.isOpen()) {
                    openPipe(pipes);
                }
                cmdlen = pipes.getUsbPipeWrite().syncSubmit(next);
            }
        } catch (Exception ex) {
            //System.out.println("Error while sending command " + next + " : " + ex.getMessage());
        }
        return cmdlen;
    }

    public String readResponse() throws UsbDisconnectedException {
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
//        }
        result = "";
        byte[] readBuffer = new byte[4096];

        int nBits = 0;
        try {
            synchronized (m_usbDevice) {
                nBits = pipes.getUsbPipeRead().syncSubmit(readBuffer);
            }
            //System.out.println(nBits);
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotOpenException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }

        // 0 is now an acceptable value; it merely means that we timed out
        // waiting for input
        if (nBits < 0) {
        } else {
            try {
                result = new String(readBuffer, 0, nBits, "US-ASCII").trim();
                //System.out.println(new String(result).trim());
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    public ByteRead readBytes() throws UsbException {

        byte[] result_local = new byte[64];
        byte[] readBuffer = new byte[64];

        int nBits = 0;

        nBits = pipes.getUsbPipeRead().syncSubmit(readBuffer);



        // 0 is now an acceptable value; it merely means that we timed out
        // waiting for input
        if (nBits < 0) {
        } else if (nBits > 0) {
            result_local = readBuffer;
        } else {
            System.err.println("Timeout!");
        }

        return new ByteRead(nBits, result_local);


    }

    public void setInitialized(boolean status) {
        synchronized (isInitialized) {
            isInitialized.set(status);
            if (!status) {
                return;
            }
        }
    }

    public String popCommand(String command) {
        int index = command.indexOf("\n");
        return command.substring(0, index);
    }

    private boolean isInitializedTop() {
        return isInitialized.get();
    }

    public boolean isInitialized() {
        if (isInitializedTop() && testPipes(pipes)) {
            return true;
        } else {
            return false;
        }
    }

    public String clean(String str) {
        String clean;
        // trim whitespace and remove spaces
        clean = str.trim().replaceAll(" ", "");

        return clean;
    }

    public void InitUsbDevice(UsbDevice device) {

        try {
            UsbDeviceDescriptor descriptor;
            if (device.isUsbHub()) {
                System.out.println("Device is a hub");
                UsbHub hub = (UsbHub) device;

                for (UsbDevice child : (List<UsbDevice>) hub.getAttachedUsbDevices()) {
                    InitUsbDevice(child);
                }
            } else {

//                if (device.isConfigured()) {
//                    descriptor = device.getUsbDeviceDescriptor();
//                } else {
//                    String message = "Device is not configured.";
//                    System.out.println(message);
//                    return;
//                }
                descriptor = device.getUsbDeviceDescriptor();

                short idVendor = descriptor.idVendor();
                short idProduct = descriptor.idProduct();


                System.out.println("idVendor:" + descriptor.idVendor());
                System.out.println("idProduct:" + descriptor.idProduct());

                if (idVendor == VENDOR_ID) {

                    String manufacturerString = device.getManufacturerString();
                    String productString = device.getProductString();
                    String s = device.getSerialNumberString();
                    System.out.println("ManufacturerString " + manufacturerString);
                    System.out.println("ProductString " + productString);
                    System.out.println("SerialNumberString " + s);

                    if (manufacturerString.contains(m_manufacturer)
                            && productString.contains(m_product)) {
                        System.out.println("Trying to add device to candidate list.");
                        m_usbDeviceList.add(device);

                        System.out.println("Adding device " + manufacturerString + ":"
                                + productString + " to candidate list.");
                    }//else{System.out.println("No need for else.");}


                }

            }
        } catch (Exception ex) {
            String message = "Could not verify or add device:"
                    + ex.getMessage() + ":" + ex.toString();
            System.out.println(message);
        }

    }

    public void InitUsbDevice() {
        m_usbDeviceList.clear();

        try {
            System.out.println("Getting device list.");

            UsbServices services = UsbHostManager.getUsbServices();
            UsbHub rootHub = services.getRootUsbHub();


            InitUsbDevice(rootHub);


        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            setInitialized(false);
        }

        int n = m_usbDeviceList.size();

        //connect to the first device available
        if (n > 1) {
            m_usbDevice = m_usbDeviceList.get(0);
            System.out.println("Multiple machines found connecting to the "
                    + "most recently connected one");
        } else if (n == 1) {
            m_usbDevice = m_usbDeviceList.get(0);
            System.out.println(((AbstractDevice) m_usbDevice).setActiveUsbConfigurationNumber());
            //System.out.println("Connecting to machine using usb device: "+ m_usbDevice.getProductString());
        } else {
            System.out.println("Failed to find USB device.");
            setInitialized(false);
        }

    }

    /**
     * Returns the usb pipes or makes a new one
     *
     * @return
     */
    protected UsbPipes GetPipe(UsbDevice device) {

        UsbConfiguration config = device.getActiveUsbConfiguration();
        byte b = 01;
        DeviceHandle dh = null;
        Device d;

        //LibUsb.open((Device)device, dh);
        //LibUsb.setConfiguration(device, countRead);
        // UsbConfiguration config = device.getUsbConfiguration(b);
        ///Assert.assertNotNull(config);
        if (pipes != null && testPipes(pipes)) {
            return pipes;
        } else {
            pipes = new UsbPipes();
        }
        System.out.println("Configs" + device.getUsbConfigurations());
        List interfaces = config.getUsbInterfaces();
        for (Object ifaceObj : interfaces) {
            UsbInterface iface = (UsbInterface) ifaceObj;
            if (iface.isClaimed() || !iface.isActive()) {
                continue;
            }

            List endpoints = iface.getUsbEndpoints();
            for (Object endpointObj : endpoints) {
                UsbEndpoint endpoint = (UsbEndpoint) endpointObj;

                if (endpoint.getType() != UsbConst.ENDPOINT_TYPE_BULK) {
                    continue;
                }

                if (endpoint.getDirection() == UsbConst.ENDPOINT_DIRECTION_OUT) {
                    this.pipes.setUsbPipeWrite(endpoint.getUsbPipe());
                }

                if (endpoint.getDirection() == UsbConst.ENDPOINT_DIRECTION_IN) {
                    this.pipes.setUsbPipeRead(endpoint.getUsbPipe());
                }
            }
            if (pipes.getUsbPipeRead() != null
                    && pipes.getUsbPipeWrite() != null) {

                if (testPipes(pipes)) {
                    return pipes;
                } else {
                    continue;
                }

            } else {
                pipes.setUsbPipeWrite(null);
                pipes.setUsbPipeRead(null);
            }
        }

        return null;
    }

    protected boolean testPipes(UsbPipes pipes) {
        //Acredito no coração dos pipes

//        // Confirm the USB device it's ok and working properly
        if (pipes.getUsbPipeWrite() == null || pipes.getUsbPipeRead() == null) {
            try {
                pipes.close();
            } catch (UsbException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UsbNotActiveException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UsbNotOpenException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UsbDisconnectedException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return true;
    }

    /**
     *
     * @param pipe
     * @throws UsbClaimException
     * @throws UsbNotActiveException
     * @throws UsbDisconnectedException
     * @throws UsbException
     */
    protected void openPipe(UsbPipes pipes) {
        try {
            if (!pipes.getUsbEndpoint().getUsbInterface().isClaimed()) {
                pipes.getUsbEndpoint().getUsbInterface().claim();
            }
            if (!pipes.isOpen()) {
                pipes.open();
            }
            setInitialized(true);
        } catch (UsbClaimException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param pipe
     * @throws UsbClaimException
     * @throws UsbNotActiveException
     * @throws UsbDisconnectedException
     * @throws UsbException
     */
    protected void closePipe(UsbPipes pipes) {
        try {
            setInitialized(false);
            pipes.close();
            pipes.getUsbEndpoint().getUsbInterface().release();
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotOpenException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    String flashAndCheck(String filename, int nBytes) {

        FileInputStream in = null;
        long loop;
        int file_size;
        int c = 0;
        int sent;
        ByteRead res;
        boolean state;
        String command;

        String out = "";

        File f = new File(filename);

        if (!f.isFile() || !f.canRead()) {
            return out + "File not found or unreadable.\n";
        }

        file_size = (int) f.length();

        try {
            in = new FileInputStream(f);
        } catch (FileNotFoundException ex) {
            return out + "File not found or unreadable.\n";
        }

        command = "M650 A" + file_size;

        out += "File: " + filename + "; size:" + file_size + ".\n";
        out += "Sending: " + command + "\n";

        sendCommand(command);

        //sleep for a nano second just for luck
        try {
            Thread.sleep(0, 1);
        } catch (InterruptedException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }

        String response;
        out += (response = readResponse()) + "\n";

        //test for ok in response
        if (!response.toLowerCase().contains(RESPONSE_OK)) {
            return out + "M650 failed. Response not OK";
        }

        out += "Sending \n";

        loop = System.nanoTime();
        int bytesRead = 0;

        try {
            byte[] byteTemp = new byte[64];
            ByteRead byteMessage;
            byteMessage = new ByteRead(64, new byte[0]);
            while (((byteMessage.size = in.read(byteTemp)) != -1)
                    && (nBytes == -1 || (bytesRead < nBytes))) {
                bytesRead += byteMessage.size;
                byteMessage = new ByteRead(byteMessage.size, new byte[byteMessage.size]);
                System.arraycopy(byteTemp, 0, byteMessage.byte_array, 0, byteMessage.size);

                sent = sendCommandBytes(byteMessage.byte_array);

                try {
                    Thread.sleep(0, 1); //sleep for a nano second just for luck
                } catch (InterruptedException ex) {
                    Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (sent == 0) {
                    out += "Sent " + sent + " of " + byteMessage.size + " bytes.\n";
                    return out + "Transfer failure.\n";
                }

                out += "Sent " + sent + " of " + byteMessage.size + " bytes.\n";
                out += "Verifying\n";
//                System.out.println(out);

                int tries = 3;

                res = new ByteRead(0, new byte[byteMessage.size]);
                while (tries > 0) {
                    try {
                        tries--;

                        ByteRead tempMessage = readBytes();
                        System.arraycopy(tempMessage.byte_array, 0,
                                res.byte_array, res.size, tempMessage.size);
                        res.size += tempMessage.size;

                        if (res.size == byteMessage.size) {
                            state = Arrays.equals(res.byte_array, byteMessage.byte_array);
                            if (!state) {
                                return out += "Transmission error found, reboot R2C2.\n";
                            } else {
                                out += "ok\n";
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        if (!(tries > 0)) {
                            return out += "Timeout after " + tries + ".\n";
                        }
                        Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }

        loop = System.nanoTime() - loop;
        return out += "Transmission sucessfull " + file_size + "bytes in " + loop / 1000000000.0
                + "s : " + file_size / (loop / 1000000.0) + "kbps\n";
    }

    String printGcode(String filename) throws IOException {
        BufferedReader br = null;

        String command;

        String out = "";

        File f = new File(filename);

        if (!f.isFile() || !f.canRead()) {
            return out + "File not found or unreadable.\n";
        }

        br = new BufferedReader(new FileReader(f));

        while ((command = br.readLine()) != null) {
            sendCommand(command);

            while (!readResponse().toLowerCase().contains("ok")) {
            }
        }

        br.close();

        return out += "Job's done!";
    }

    String checkFirmware(String filename) {

        FileInputStream in;

        long loop;
        int file_size;
        boolean state;
        String command;
        String out = "";
        byte[] file_bytes;

        File f = new File(filename);
        if (!f.isFile() || !f.canRead()) {
            return out + "File not found or unreadable.\n";
        }

        file_size = (int) f.length();
        try {
            in = new FileInputStream(f);
            file_bytes = new byte[file_size];
            in.read(file_bytes);
        } catch (FileNotFoundException ex) {
            return out += "Could not open file:" + filename + "\n";
        } catch (IOException ex) {
            return out += "Could not read file:" + filename + "\n";
        }

        command = "M652 A" + file_size;

        out += "File: " + filename + "; size:" + file_size + ".\n";
        out += "Sending: " + command + "\n";

        byte[] response_bytes = new byte[file_size];
        int n_bytes = 0;
        int tries = 10;
        loop = System.nanoTime();

        sendCommand(command);

        String response;
        out += (response = readResponse()) + "\n";

        //test for ok in response
        if (!response.toLowerCase().contains(RESPONSE_OK)) {
            return out + "M652 failed. Response not OK";
        }

        while (n_bytes < file_size && tries > 0) {
            try {
                Thread.sleep(0, 1); //sleep for a nano second just for luck
                ByteRead res = readBytes();
                System.arraycopy(res.byte_array, 0, response_bytes, n_bytes, res.size);
                n_bytes += res.size;
            } catch (Exception e) {
                if (!(tries > 0)) {
                    return out += "Timeout after " + tries + ".\n";
                }
                tries--;
            }

        }

        loop = System.nanoTime() - loop;
        state = Arrays.equals(response_bytes, file_bytes);

        if (state) {
            return out += "Check sucessfull: " + filename + ":" + file_size + " is ok.\n"
                    + "t: " + loop / 1000000000 + "\n"
                    + "bitrate: " + file_size / (loop / 1000000) + "kbps\n";
        } else {
            return out += "Check Failed!\n";
        }
    }

    String flush() {
        int counter = 0;
        while (readResponse().length() > 0) {
            counter += readResponse().length();

            if (counter % 4096 == 0) {
                System.out.println("vai em " + counter);
            }

        }

        return "Clear " + counter + " chars.";
    }

    String testFirmware() throws InterruptedException {


        String G28 = "G28";
        String status = "M630";
        String G1A = "G1 F100 X0 Y0 Z0 E0";
        String G1B = "G1 F100 X100 Y100 Z100 E100";
        String G1C = "G1 F100 X-100 Y-100 Z-100 E-100";

        String READY = "S:3";
        String BUSY = "S:4";
        String ERROR = "S:0";




        sendCommand(status);
        System.out.println(status);
        for (String out = readResponse(); !out.toLowerCase().contains(READY);) {
            System.out.println(out);
            Thread.sleep(50, 0); //sleep for a nano second just for luck
            sendCommand(status);
        }


        sendCommand(G28);
        System.out.println(G28);
        for (String out = readResponse(); !out.toLowerCase().contains(READY);) {
            System.out.println(out);
            Thread.sleep(50, 0); //sleep for a nano second just for luck
            sendCommand(status);
        }

        sendCommand(G1A);
        System.out.println(G1A);
        for (String out = readResponse(); !out.toLowerCase().contains(READY);) {
            System.out.println(out);
            Thread.sleep(50, 0); //sleep for a nano second just for luck
            sendCommand(status);
        }

        sendCommand(G1B);
        System.out.println(G1B);
        for (String out = readResponse(); !out.toLowerCase().contains(READY);) {
            System.out.println(out);
            Thread.sleep(50, 0); //sleep for a nano second just for luck
            sendCommand(status);
        }


        sendCommand(G1C);
        System.out.println(G1C);
        for (String out = readResponse(); !out.toLowerCase().contains(READY);) {
            System.out.println(out);
            Thread.sleep(50, 0); //sleep for a nano second just for luck
            sendCommand(status);
        }


        return "Job's done!";

    }

    void logTemp(double target, int durationSecs, int tick) {

        long start = new Date().getTime();
        long next = new Date().getTime();
        long current = new Date().getTime();
        long finish = start + durationSecs * 1000;

        String getTemp = "M105";
        String setTemp = "M104 S" + target;
        String response = "";

        String re1 = "(T)";	// Variable Name 1
        String re2 = "(:)";	// Any Single Character 1
        String re3 = "([+-]?\\d*\\.\\d+)(?![-+0-9\\.])";	// Float 1
        String re4 = ".*?";	// Non-greedy match on filler
        String re5 = "(ok)";	// Word 1
        Pattern p = Pattern.compile(re1 + re2 + re3 + re4 + re5, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m;
        String value;
        String newLine = System.getProperty("line.separator");
        FileWriter writer;

        sendCommand(setTemp);
        response = readResponse();

        try {
            writer = new FileWriter("to" + (int)target + "in" + durationSecs + ".txt");
            writer.write("---------------" + newLine);
            writer.write("tick" + "\t" + "target" + newLine);
            writer.write(tick + "\t" + target + newLine);
            writer.write("---------------" + newLine);

            while (current < finish) {

                if (current > next) {
                    sendCommand(getTemp);
                    response = readResponse();
                    next = next + tick;
                    m = p.matcher(response);

                    if (m.find()) {
                        value = m.group(3);

                        writer.write("" + (current - start) / 100 + "\t" + value + newLine);
                    }

                }
                current = new Date().getTime();

            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
