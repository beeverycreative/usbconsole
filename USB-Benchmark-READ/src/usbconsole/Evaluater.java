package usbconsole;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author mdomingues
 */
public class Evaluater {
    
    AtomicBoolean sent = new AtomicBoolean(false);
    String command_Sent = "";
    String command_received = "";
    private long start;  
    private int commands = 0;

    public Evaluater() {
        start = System.currentTimeMillis();
    }
    
    public boolean sent() {
        return sent.get();
    }

    public void setSent(boolean ready) {
        this.sent.set(ready);
    }

    public void setCommand_Sent(String command_Sent) {
        this.command_Sent = command_Sent;
    }

    public void setCommand_received(String command_received) {
        commands++;
        this.command_received = command_received;
    }
    
    public boolean okToGo()
    {
        
        //System.out.println("S="+command_Sent+" "+"R="+command_received);
        //if (commands ==1){
//        long now = System.currentTimeMillis();
//        //System.out.println("passaram 1 em "+(now-start));
//        commands=0;
//        start = now;
        //}
        return command_Sent.equals(command_received);
    }
}
