package usbconsole;

/**
 *
 * @author mdomingues
 */
public class Tester extends Thread {

    private USBConsole console;
    private NewJFrame frame;
    private Evaluater ev;
    private ByteRead result;
    private byte current_byte = '0';
    private int message_number;
    private int total_bytes;
    private boolean started = false;
    private int ok_bytes;
    private int expected_bytes= 1024;
    private long start;
    private long total;
    private long loop;
    
    public Tester(USBConsole console, NewJFrame frame, Evaluater ev) {
        this.console = console;
        this.frame = frame;
        this.ev = ev;
    }

    @Override
    public void run() {       
        total = 0;
        while (total_bytes< expected_bytes) {

            String command = null;
//             if (!ev.okToGo()) {
               // Thread.sleep(1);
                
                loop = System.currentTimeMillis();
                result = console.readBytes();
                loop = System.currentTimeMillis() - loop;
                total += loop;
//            } 
            if(!(result.size < 0)) {
                ok_bytes = 0;
                message_number++;
                total_bytes+=result.size;
                
                if (!started){
                    current_byte = result.byte_array[0];
                    current_byte--;
                    started = true;
                }
               
                for(int i=0; i< result.size; i++ ){
                   if (result.byte_array[i] == current_byte+1){
                        current_byte = result.byte_array[i];
                        ok_bytes++;
                   }
                }
                
                System.out.println("^:"+loop +" t:"+total+" m:"+message_number+"byte: "+ok_bytes+"/"+result.size);
               
                //frame.updateOutput(command);
            }
        }
    }
}
