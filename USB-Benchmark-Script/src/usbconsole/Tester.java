package usbconsole;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdomingues
 */
public class Tester extends Thread {

    private USBConsole console;
    private NewJFrame frame;
    private Evaluater ev;
    private int messageSize = 64;
    private byte current_byte = '0';
    private int message_number;
    private int total_bytes;
    private boolean started = false;
    private int ok_bytes;
    private int expected_bytes = 1024;
    private long start;
    private long total;
    private long loop;
    private long send;
    private long read;
    private String comResponse;

    public Tester(USBConsole console, NewJFrame frame, Evaluater ev) {
        this.console = console;
        this.frame = frame;
        this.ev = ev;
    }

    @Override
    public void run() {
        total = 0;
        byte[] byteMessage = new byte[64];

        FileInputStream in = null;
        String filename = "FIRMWARE.bin";
        int file_size = 0;
        int c = 0;
        byte[] file_array = null;

        
        

        try {
             File f = new File(filename);         
            file_size = (int) f.length();
            in = new FileInputStream(f);
            file_array = new byte[file_size];
            in.read(file_array);
            f = new File(filename);         
            file_size = (int) f.length();
            in = new FileInputStream(f);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }
            


        System.out.println("File: " + filename + "; size:" + file_size + ";");

        String command = "Sending: M650 A" + file_size + "\n";
        System.out.println(command);
        int sent;
        ByteRead res;
        ByteRead response;
        boolean state;
        int bytesRead = 0;


        console.sendCommand(command);
        try {
            Thread.sleep(0, 1);
        } catch (InterruptedException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }

        comResponse = console.readResponse();
        // while (response.length() > 0) {
        System.out.printf(comResponse + '\n');
        /*
        if (response.toLowerCase().contains("ok")) {
        System.out.println("ok received");
        break;
        }
        response = console.readResponse();
        
        }
         */
        System.out.printf("Sending \n");
        loop = System.nanoTime();
        response = new ByteRead(0, new byte[64]);
        int counter = 0;
        int tries;
        try {
            while ((c = in.read(byteMessage)) != -1) {
                response = new ByteRead(0, new byte[64]);
                counter++;

                sent = console.sendCommandBytes(byteMessage);
               /* try {
                    Thread.sleep(0, 1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
                }*/
                System.out.printf("Sent " + c + " of " + sent + " bytes.\n");
                if (sent == 0) {
                    break;
                }

                tries = 10;

                while (tries > 0 && bytesRead < file_size) {
                    try {
                        tries--;
                        res = console.readBytes();
                        bytesRead += res.size;
                        System.out.println("R:" + res.size);
                        System.out.println(response.size);

                        System.arraycopy(res.byte_array, 0, response.byte_array, response.size, 64 - response.size);
                        response.size += res.size;

                        System.out.println(response.size);

                        if (res.size == 64 || response.size == 64) {
                            state = Arrays.equals(res.byte_array, byteMessage)
                                    || Arrays.equals(response.byte_array, byteMessage);
                            System.out.println(state);
                            if (!state) {
                                System.exit(-1);
                            } else {
                                break;
                            }
                        }

                    } catch (Exception en) {
                        if (!(tries > 0)) {
                            System.out.printf("Timeout after " + tries + ".\n");
                            System.exit(-1);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        state = Arrays.equals(response.byte_array, byteMessage);
        if (!state) {
            System.exit(-1);
        }

        send = System.nanoTime() - loop;
        System.out.printf(".\nFinished. Messages:" + counter + "; Time: " + send / 1000.0 + "milis;" + "Speed: " + file_size / (send / 1000000.0) + "KBps;\n");


        //////////////////////////////////////////


        bytesRead = 0;
        byte[] teste_file = new byte[file_size];

        while (bytesRead < file_size) {

                res = console.readBytes();
                
                System.arraycopy(res.byte_array, 0, teste_file, bytesRead, res.size);
                bytesRead += res.size;
                System.out.printf(res.size + "/" +  bytesRead+"\n");
        }



        for (int i = 0; i< file_size; i++){
            if(!(teste_file[i]==file_array[i])){
                System.out.println("ERROR:" + i + " " + (int)teste_file[i] + " " + (int)file_array[i]);
            }
                
                
        }
        
        if (!state) {
            System.out.println(Arrays.equals(teste_file, file_array));
            System.exit(-1);
        }

*/

        ///////////////////////////////////////

        command = "M630 N2\n";
        System.out.println(command);
        console.sendCommand(command);
        try {
            Thread.sleep(1000, 1);
        } catch (InterruptedException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }
        tries = 5;

        while (tries > 0) {

            try {
                String result = console.readResponse();
                tries--;
                System.out.printf(result + "\n");
                if (result.toLowerCase().contains("ok")) {
                    break;
                }

            } catch (Exception en) {
                if (!(tries > 0)) {
                    System.out.printf("Timeout after " + tries + ".\n");
                    System.exit(-1);
                }
            }
        }



        System.exit(0);
    }
}



//        while (true || total_bytes< expected_bytes) {
//
//           // byteMessage = messageGenerator();
//            new Random().nextBytes(byteMessage);
//            ByteRead result = null;
//            int tries = 5;
//
//                loop = System.nanoTime();
//                console.sendCommandBytes(byteMessage);
//                send = System.nanoTime()- loop;
//                read = 0;
//                
//                while (tries > 0 && !message_ok(result)){
//
//                    try {
//                        loop = System.nanoTime();
//                    result = console.readBytes();
//                    read = System.nanoTime()- loop;
//                    tries--;
//                    
//                    }catch(Exception en){
//                    ;
//                    }
//                }
//                total += (read + send);
////            } 
//
//                
//               System.out.println("^:"+send +" v:"+read+" t:"+total+" m:"+message_number+" byte:"+result.size+" %:"+ok_bytes+"/"+result.size);
//                
//                //frame.updateOutput(command);
//            
//        }
//    }
//
//    private boolean message_ok(ByteRead result) {
//        int count_ok=0;
//        
//        if (messageSize==result.size){
//            for(int i = 0; i<messageSize;i++)
//                if (byteMessage[i]==result.byte_array[i])
//                    count_ok++;
//            
//            if (count_ok==messageSize){
//                return true;
//            }  
//        }//No need for else
//        return false;    


