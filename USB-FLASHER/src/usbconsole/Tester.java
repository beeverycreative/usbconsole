package usbconsole;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mdomingues
 */
public class Tester extends Thread {

    private USBConsole console;
    private NewJFrame frame;
    private Evaluater ev;

    private int messageSize = 64;
    
    private byte[] byteMessage;
    private byte current_byte = '0';
    private int message_number;
    private int total_bytes;
    private boolean started = false;
    private int ok_bytes;
    private int expected_bytes= 1024;
    private long start;
    private long total;
    private long loop;
    private long send;
    private long read;
    
    public Tester(USBConsole console, NewJFrame frame, Evaluater ev) {
        this.console = console;
        this.frame = frame;
        this.ev = ev;
    }

    @Override
    public void run() {       
        total = 0;
        FileInputStream in = null;
        String filename = "firmware.bin";
        long file_size = 0;
        int c;
        try {               
            file_size = new File(filename).length();
            in = new FileInputStream(filename);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);

        }
              
        System.out.println("File: "+filename+"; size:"+ file_size+";");
        
        
        System.out.println("Sending: M650 A"+file_size+"\n");
        try {
            console.sendCommand("M650 A"+file_size+"\n");
            if(!console.readResponse().toLowerCase().contains("ok")){
                System.out.println("Error: tried to send M650, no ok received");
                System.exit(-1);
            }
            
            System.out.printf("Sending ");
            loop = System.nanoTime();
            while ((c = in.read(byteMessage)) != -1) {
                console.sendCommandBytes(byteMessage);
                System.out.printf(".");
            }
            send = System.nanoTime() - loop;
            System.out.printf(".\nFinished. Time: "+send/1000+"milis;"+"Speed: "+file_size/(send/1000)+"KBps;");
            
        } catch (IOException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        System.exit(0);
        
        
        
//        while (true || total_bytes< expected_bytes) {
//
//           // byteMessage = messageGenerator();
//            new Random().nextBytes(byteMessage);
//            ByteRead result = null;
//            int tries = 5;
//
//                loop = System.nanoTime();
//                console.sendCommandBytes(byteMessage);
//                send = System.nanoTime()- loop;
//                read = 0;
//                
//                while (tries > 0 && !message_ok(result)){
//
//                    try {
//                        loop = System.nanoTime();
//                    result = console.readBytes();
//                    read = System.nanoTime()- loop;
//                    tries--;
//                    
//                    }catch(Exception en){
//                    ;
//                    }
//                }
//                total += (read + send);
////            } 
//
//                
//               System.out.println("^:"+send +" v:"+read+" t:"+total+" m:"+message_number+" byte:"+result.size+" %:"+ok_bytes+"/"+result.size);
//                
//                //frame.updateOutput(command);
//            
//        }
//    }
//
//    private boolean message_ok(ByteRead result) {
//        int count_ok=0;
//        
//        if (messageSize==result.size){
//            for(int i = 0; i<messageSize;i++)
//                if (byteMessage[i]==result.byte_array[i])
//                    count_ok++;
//            
//            if (count_ok==messageSize){
//                return true;
//            }  
//        }//No need for else
//        return false;    
        
        
    }
}
