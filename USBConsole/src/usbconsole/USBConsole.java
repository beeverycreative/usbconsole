package usbconsole;


import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.usb.UsbClaimException;
import javax.usb.UsbConfiguration;
import javax.usb.UsbConst;
import javax.usb.UsbDevice;
import javax.usb.UsbDeviceDescriptor;
import javax.usb.UsbDisconnectedException;
import javax.usb.UsbEndpoint;
import javax.usb.UsbException;
import javax.usb.UsbHostManager;
import javax.usb.UsbHub;
import javax.usb.UsbInterface;
import javax.usb.UsbNotActiveException;
import javax.usb.UsbNotOpenException;
import javax.usb.UsbServices;
import static usbconsole.USBConsole.m_usbDevice;

/**
 *
 * @author bitbox
 */
public class USBConsole {

    protected static UsbDevice m_usbDevice;
    protected ArrayList<UsbDevice> m_usbDeviceList = new ArrayList<UsbDevice>();
    private String m_manufacturer = "BEEVERYCREATIVE";
    private String m_product = "BEETHEFIRST - firmware";
    /**
     * Lock for multi-threaded access to this driver's serial port.
     */
    private final ReentrantReadWriteLock m_usbLock = new ReentrantReadWriteLock();
    /**
     * Locks the serial object as in use so that it cannot be disposed until it
     * is unlocked. Multiple threads can hold this lock concurrently.
     */
    public final ReentrantReadWriteLock.ReadLock m_usbInUse = m_usbLock.readLock();
    protected UsbPipes pipes;
    /**
     * What did we get back from serial?
     */
    private String result = "";
    private DecimalFormat df;
    private int countRead = 0;
    private boolean showMessage = true;
    private AtomicBoolean isInitialized = new AtomicBoolean(false);

    public void initialize() {

        // wait till we're initialized
        if (!isInitialized()) {
            // record our start time.
            Date date = new Date();
            //long end = date.getTime() + 10000;

            System.out.println("Initializing usb device.");
            while (!isInitialized()) {
                try {
                    InitUsbDevice();
                    if (m_usbDevice != null) {
                        pipes = GetPipe(m_usbDevice);

                        showMessage = false;
                    } else {
                        setInitialized(false);
                        if (showMessage) {
                            System.out.println("Error while initializing device. Device not present. ");
                        }
                    }
                    openPipe(pipes);
                } catch (Exception e) {
                    try {
                        Thread.sleep(500); // sleep 500 ms
                    } catch (InterruptedException ex) {
                        System.out.println("error while sleeping in open usb connection initialization: " + ex.getMessage());
                    }
                }

                // record our current time
                date = new Date();
                long now = date.getTime();


                if (showMessage) {
                    System.out.println("usb device non-responsive. Please plug in the USB device !");
                    //Stop showing the message 1 time is enough :) 
                    showMessage = false;
                }
            }

            try {
                /* if (pipes != null) {
                 closePipe(pipes);
                 }*/
                System.out.println("Ready.");
            } catch (Exception ex) {
                System.out.println("error while finalizing the usb connection initialization: " + ex.getMessage());
                // setInitialized(false);
                setInitialized(false);
                return;
            } finally {
                // Messages could come back later
                showMessage = true;
            }
        }
        //sendCommand("M300");
        // default us to absolute positioning
        //sendCommand("G28");
    }

    protected boolean pipesAvailable() {
        // Grab a lock
        m_usbLock.writeLock().lock();

        UsbDevice newDevice = null;
        try {
            UsbServices services = UsbHostManager.getUsbServices();
            UsbHub rootHub = services.getRootUsbHub();
            List devices = rootHub.getAttachedUsbDevices();

            // select correct device
            for (Object deviceObj : devices) {
                newDevice = (UsbDevice) deviceObj;

                if (newDevice.getManufacturerString().trim().contains(m_manufacturer)
                        && newDevice.getProductString().trim().contains(m_product)) {
                    System.out.println("Connecting to machine using usb device: " + newDevice.getProductString());
                    m_usbDevice = newDevice;
                    break;
                }
            }
        } catch (Exception e) {
            String message = "Error finding USB device: " + e.getMessage();
            System.out.println(message);
            return false;
            //setError(message);
        } finally {
            setInitialized(false);
            m_usbDevice = newDevice;
            m_usbLock.writeLock().unlock();
        }

        return true;
    }

    protected int sendCommand(String next) {

        next = clean(next);
        int cmdlen = 0;

        // skip empty commands.
        if (next.length() == 0) {
            return 0;
        }

        pipes = GetPipe(m_usbDevice);

        // do the actual send.
        String message = next + "\n";


        try {
            synchronized (m_usbDevice) {
                if (!pipes.isOpen()) {
                    openPipe(pipes);
                }

                pipes.getUsbPipeWrite().syncSubmit(message.getBytes());

                cmdlen = next.length() + 1;
            }
        } catch (Exception ex) {
            System.out.println("Error while sending command " + next + " : " + ex.getMessage());
        }
        return cmdlen;
    }

    /**
     * Send command to Machine
     *
     * @param next Command
     * @return command length
     */
    protected int sendCommandBytes(byte[] next) {

        int cmdlen = 0;
        int i = 0;

        pipes = GetPipe(m_usbDevice);

        try {
            synchronized (m_usbDevice) {
                if (!pipes.isOpen()) {
                    openPipe(pipes);
                }
                pipes.getUsbPipeWrite().syncSubmit(next);
                cmdlen = next.length;
            }
        } catch (Exception ex) {
            //System.out.println("Error while sending command " + next + " : " + ex.getMessage());
        }
        return cmdlen;
    }

    public String readResponse() throws UsbDisconnectedException {
        result = "";
        byte[] readBuffer = new byte[64];

        int nBits = 0;
        try {
            synchronized (m_usbDevice) {
                nBits = pipes.getUsbPipeRead().syncSubmit(readBuffer);
            }
            //System.out.println(nBits);
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotOpenException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }

        // 0 is now an acceptable value; it merely means that we timed out
        // waiting for input
        if (nBits < 0) {
            // This signifies EOF. FIXME: How do we handle this?
            //System.out.println(USBConsole.class.getName() + ": EOF occured");
            //result = USBConsole.class.getName() + ": EOF occured";
            // return result;
        } else {
            try {
                result = new String(readBuffer, 0, nBits, "US-ASCII").trim();
                //System.out.println(new String(result).trim());
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            }
//            int index;
//            while ((index = result.indexOf('\n')) >= 0) {
//                String line = result.substring(0, index).trim();
//                result = result.substring(index + 1);
//                if (line.length() == 0) {
//                    continue;
//                }
//                //System.out.println("Linha =" + line);
//                result = line;
//            }

        }

        return result;
    }

    public ByteRead readBytes() {

        byte[] result_local = new byte[64];
        byte[] readBuffer = new byte[64];

        int nBits = 0;
        try {
            nBits = pipes.getUsbPipeRead().syncSubmit(readBuffer);
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotOpenException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }


        // 0 is now an acceptable value; it merely means that we timed out
        // waiting for input
        if (nBits < 0) {
            // This signifies EOF. FIXME: How do we handle this?
            //System.out.println(USBConsole.class.getName() + ": EOF occured");
            //result = USBConsole.class.getName() + ": EOF occured";
            // return result;
        } else if (nBits > 0) {
            result_local = readBuffer;
        } else {
            System.err.println("IMPOSSIBURI!!!");
        }

        return new ByteRead(nBits, result_local);


    }

    public void setInitialized(boolean status) {
        synchronized (isInitialized) {
            isInitialized.set(status);
            if (!status) {
                return;
            }
        }
    }

    public String popCommand(String command) {
        int index = command.indexOf("\n");
        return command.substring(0, index);
    }

    private boolean isInitializedTop() {
        return isInitialized.get();
    }

    public boolean isInitialized() {
        if (isInitializedTop() && testPipes(pipes)) {
            return true;
        } else {
            return false;
        }
    }

    public String clean(String str) {
        String clean = str;

        // trim whitespace
        clean = clean.trim();

        // remove spaces
        clean = clean.replaceAll(" ", "");

        return clean;
    }

    public void InitUsbDevice(UsbDevice device) {

        UsbDeviceDescriptor desc = device.getUsbDeviceDescriptor();
        try {        
            String manufacturerString = device.getManufacturerString();
            String productString = device.getProductString();
           
            if (manufacturerString.contains(m_manufacturer)
                    && productString.contains(m_product)) {
                System.out.println("Trying to add device to candidate list.");
                m_usbDeviceList.add(device);
                System.out.println("Adding device to candidate list.");
            }//else{System.out.println("No need for else.");}

        } catch (Exception ex) {
            String message = "Could not verify or add device:" + ex.getMessage()
                    + ":" + ex.toString();
            System.out.println(message);
        }
        if (device.isUsbHub()) {
            System.out.println("Device is a hub");
            UsbHub hub = (UsbHub) device;
            for (UsbDevice child : (List<UsbDevice>) hub.getAttachedUsbDevices()) {
                InitUsbDevice(child);
            }
        } //else { System.out.println("No need for else");}
    }

    public void InitUsbDevice() {
        
        try {       
            System.out.println("Getting device list.");
            
            UsbServices services = UsbHostManager.getUsbServices();
            UsbHub rootHub = services.getRootUsbHub();
            
            InitUsbDevice(rootHub);                 
                      
            int n = m_usbDeviceList.size();
            
             //connect to the first device available
            if(n > 1){
                m_usbDevice = m_usbDeviceList.get(0);
                System.out.println("Multiple machines found connecting to the most recently connected one");
            }else if (n == 1){
                m_usbDevice = m_usbDeviceList.get(0);
                System.out.println("Connecting to machine using usb device: " + m_usbDevice.getProductString());
            }else{
                System.out.println("Failed to find USB device.");
            }         
            
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } finally {setInitialized(false);}         
    }
    /**
     * Returns the usb pipes or makes a new one
     *
     * @return
     */
    protected UsbPipes GetPipe(UsbDevice device) {
        UsbConfiguration config = device.getActiveUsbConfiguration();
        ///Assert.assertNotNull(config);
        if (pipes != null && testPipes(pipes)) {
            return pipes;
        } else {
            pipes = new UsbPipes();

        }
        List interfaces = config.getUsbInterfaces();
        for (Object ifaceObj : interfaces) {
            UsbInterface iface = (UsbInterface) ifaceObj;
            if (iface.isClaimed() || !iface.isActive()) {
                continue;
            }

            List endpoints = iface.getUsbEndpoints();
            for (Object endpointObj : endpoints) {
                UsbEndpoint endpoint = (UsbEndpoint) endpointObj;

                if (endpoint.getType() != UsbConst.ENDPOINT_TYPE_BULK) {
                    continue;
                }

                if (endpoint.getDirection() == UsbConst.ENDPOINT_DIRECTION_OUT) {
                    this.pipes.setUsbPipeWrite(endpoint.getUsbPipe());
                }

                if (endpoint.getDirection() == UsbConst.ENDPOINT_DIRECTION_IN) {
                    this.pipes.setUsbPipeRead(endpoint.getUsbPipe());
                }
            }
            if (pipes.getUsbPipeRead() != null && pipes.getUsbPipeWrite() != null) {

                if (testPipes(pipes)) {
                    return pipes;
                } else {
                    continue;
                }

            } else {
                pipes.setUsbPipeWrite(null);
                pipes.setUsbPipeRead(null);
            }
        }

        return null;
    }

    protected boolean testPipes(UsbPipes pipes) {
        //Acredito no coração dos pipes

//        // Confirm the USB device it's ok and working properly
        if (pipes.getUsbPipeWrite() == null || pipes.getUsbPipeRead() == null) {
            try {
                pipes.close();
            } catch (UsbException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UsbNotActiveException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UsbNotOpenException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UsbDisconnectedException ex) {
                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return true;
//
//        UsbIrp usbIrp = pipes.getUsbPipeWrite().createUsbIrp();
//        //usbIrp.setData(" M300 S0 P50\n ".getBytes());
//        UsbIrp readUsbIrp = pipes.getUsbPipeRead().createUsbIrp();
//        readUsbIrp.setAcceptShortPacket(true);
//        try {
//            if (!pipes.getUsbPipeRead().getUsbEndpoint().getUsbInterface().isClaimed()) {
//                pipes.getUsbPipeRead().getUsbEndpoint().getUsbInterface().claim();
//            }
//            if (!pipes.isOpen()) {
//                pipes.open();
//            }
//            pipes.getUsbPipeWrite().syncSubmit(usbIrp);
//        } catch (UsbException ex) {
//            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        } catch (UsbNotActiveException ex) {
//            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        } catch (UsbNotOpenException ex) {
//            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        } catch (IllegalArgumentException ex) {
//            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (UsbDisconnectedException ex) {
//            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
//
//        readUsbIrp.setData(new byte[64]);
//        int i = 0;
//        while (i >= 3) {
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            try {
//                pipes.getUsbPipeRead().syncSubmit(readUsbIrp);
//                i++;
//            } catch (Exception ex) {
//                try {
//                    Thread.sleep(255);
//                } catch (InterruptedException ex1) {
//                    Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex1);
//                }
//                //System.out.println("sleep");
//            }
//        }
//        if (i <= 3) {
//            return true;
//        } else {
//            //meaning: the ok signal wasn't read !! continues for the next pipes available
//            return false;
//        }

    }

    /**
     *
     * @param pipe
     * @throws UsbClaimException
     * @throws UsbNotActiveException
     * @throws UsbDisconnectedException
     * @throws UsbException
     */
    protected void openPipe(UsbPipes pipes) {
        try {
            if (!pipes.getUsbEndpoint().getUsbInterface().isClaimed()) {
                pipes.getUsbEndpoint().getUsbInterface().claim();
            }
            if (!pipes.isOpen()) {
                pipes.open();
            }
            setInitialized(true);
        } catch (UsbClaimException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param pipe
     * @throws UsbClaimException
     * @throws UsbNotActiveException
     * @throws UsbDisconnectedException
     * @throws UsbException
     */
    protected void closePipe(UsbPipes pipes) {
        try {
            setInitialized(false);
            pipes.close();
            pipes.getUsbEndpoint().getUsbInterface().release();
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotOpenException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}