package usbconsole;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bitbox
 */
public class ReadThread extends Thread {
    
    USBConsole console;
    NewJFrame frame;

    public ReadThread(USBConsole console, NewJFrame frame) {
        this.console = console;
        this.frame = frame;
    }
    
    @Override
    public void run()
    {
        String incoming_message = "";
        while(true)
        {
           if(!frame.isSending())
           {
               incoming_message = console.readResponse();
//               System.out.println(incoming_message);
               frame.setFeedbackMessage(incoming_message); 
           }
           
        }
        
    }
    
}
