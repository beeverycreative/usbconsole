package usbconsole;

import de.ailis.usb4java.topology.Usb4JavaDevice;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.usb.UsbClaimException;
import javax.usb.UsbConfiguration;
import javax.usb.UsbConst;
import javax.usb.UsbDevice;
import javax.usb.UsbDisconnectedException;
import javax.usb.UsbEndpoint;
import javax.usb.UsbException;
import javax.usb.UsbHostManager;
import javax.usb.UsbHub;
import javax.usb.UsbInterface;
import javax.usb.UsbIrp;
import javax.usb.UsbNotActiveException;
import javax.usb.UsbNotOpenException;
import javax.usb.UsbServices;
import javax.xml.bind.DatatypeConverter;
//import junit.framework.Assert;

/**
 *
 * @author bitbox
 */
public class USBConsole {

    /**
     * USB device properties
     */
    protected static Usb4JavaDevice m_usbDevice;
    private String m_manufacturer = "BEEVERYCREATIVE";
    private String m_product = "BEETHEFIRST - firmware";
    
    /**
     * Lock for multi-threaded access to this driver's serial port.
     */
    private final ReentrantReadWriteLock m_usbLock = new ReentrantReadWriteLock();
    /**
     * Locks the serial object as in use so that it cannot be disposed until it
     * is unlocked. Multiple threads can hold this lock concurrently.
     */
    public final ReentrantReadWriteLock.ReadLock m_usbInUse = m_usbLock.readLock();
    protected UsbPipes pipes;
    /**
     * To keep track of outstanding commands
     */
    private Queue<Integer> commands = new LinkedList<Integer>();
    ;
   /**
    * the size of the buffer on the GCode host TEST VALUE FOR USB
    */
   private int maxBufferSize = 60;
    /**
     * the amount of data we've sent and is in the buffer.
     */
    private int bufferSize = 0;
    /**
     * What did we get back from serial?
     */
    private String result = "";
    private DecimalFormat df;
    private byte[] readBuffer = new byte[512];
    private int countRead = 0;
    private boolean showMessage = true;
    private AtomicBoolean isInitialized = new AtomicBoolean(false);
    private AtomicBoolean g28 = new AtomicBoolean(false);

    public void initialize() {

        // wait till we're initialized
        if (!isInitialized()) {
            // record our start time.
            Date date = new Date();
            //long end = date.getTime() + 10000;

            System.out.println("Initializing usb device.");
            while (!isInitialized()) {
                try {
                    InitUsbDevice();
                    if (m_usbDevice != null) {
                        pipes = GetPipe(m_usbDevice);
                        showMessage = false;
                    } else {
                        setInitialized(false);
                        if (showMessage) {
                            System.out.println("Error while initializing device. Device not present. ");
                        }
                    }
                    openPipe(pipes);
                } catch (Exception e) {
                    try {
                        Thread.sleep(500); // sleep 500 ms
                    } catch (InterruptedException ex) {
                        System.out.println("error while sleeping in open usb connection initialization: " + ex.getMessage());
                    }
                }

                // record our current time
                date = new Date();
                long now = date.getTime();


                if (showMessage) {
                    System.out.println("usb device non-responsive. Please plug in the USB device !");
                    //Stop showing the message 1 time is enough :) 
                    showMessage = false;
                }
            }

            try {
                /* if (pipes != null) {
                 closePipe(pipes);
                 }*/
                System.out.println("Ready.");
            } catch (Exception ex) {
                System.out.println("error while finalizing the usb connection initialization: " + ex.getMessage());
                // setInitialized(false);
                setInitialized(false);
                return;
            } finally {
                // Messages could come back later
                showMessage = true;
            }
        }
        //beep
        //sendCommand("M300");
        // default us to absolute positioning
        //sendCommand("G28");
    }

    protected boolean pipesAvailable()
    {
          // Grab a lock
        m_usbLock.writeLock().lock();

        Usb4JavaDevice newDevice = null;
        try {
            UsbServices services = UsbHostManager.getUsbServices();
            UsbHub rootHub = services.getRootUsbHub();
            List devices = rootHub.getAttachedUsbDevices();

            for (int i = 0; i < devices.size(); i++) {

                //System.out.println(newDevice.getManufacturerString());

                if (devices.get(i).toString().contains("(ffff:014e)")) {
                    newDevice = (Usb4JavaDevice) devices.get(i);

                    if (newDevice.getManufacturerString().contains(m_manufacturer)
                            && newDevice.getProductString().contains(m_product)
                            && !newDevice.isUsbHub()) {
                        m_usbDevice = newDevice;
                        return true;
                    }

                }

            }

        } catch (Exception e) {
           System.out.println("Error finding USB device: " + e.getMessage());
            return false;
        } finally {
            setInitialized(false);
            //System.out.println("FINALLY ="+newDevice.toString());
            m_usbDevice = newDevice;
            m_usbLock.writeLock().unlock();
        }
        return false;
        
    }
    
    /**
     * Updates machine firmware
     * @return Success flag
     */
    protected String sendFirmware()
    {
        // Success Flag
        String operationFlag = " ";
        // Firmware File
        File firmware = new File("FIRMWARE.bin");
        // Streamer to read from FIRMWARE.bin
        DataInputStream dis = null;
        // Streamer to write from DataInputStream to ByteArray
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // Stream to control ByteArrayOutputStream
        DataOutputStream dos = new DataOutputStream(baos);
        // ByteArray with data
        byte[] data = new byte[100000];
        // Bytes read counter
        int count = -1;
        // Total_Bytes to be sent
        double total_Bytes = 0;
        
        // Fullfills DIS
        try {
             dis = new DataInputStream(new BufferedInputStream(
                                       new FileInputStream(firmware)));
        } catch (FileNotFoundException ex) {
            return "FIRMWARE.bin not available";
        }
       
        // Counts number of bytes read into the buffer
        try {
            count = dis.read(data);
            total_Bytes = count;
        } catch (IOException ex) {
            return "Cant read from FIRMWARE.bin";
        }
        
        // Reads File and stores data
        while(count != -1)
        {
            try {
                dos.write(data, 0, count);
            } catch (IOException ex) {
                return "Cant write to buffer.";
            }
            try {
                count = dis.read(data);
            } catch (IOException ex) {
                return "Cant estimate number of bytes remaining";
            }
        }

        total_Bytes = baos.toByteArray().length;  
        
        // Send start command to update FIRMWARE
        sendCommand("M650 A"+total_Bytes+" N0\n");
        
        // Sends Data in 64 bytes packets
        splitNSend(baos.toByteArray());
        
        return operationFlag;
    }
    
    /**
     * Method to split byte[] in 64 packets bytes to send over usb com channel
     * @param src source byte[]
     * @return error info
     */
    private String splitNSend(byte[] src)
    {
        // Command Byte Array to be sent
        byte[] dest = new byte[64];
        // Firmware sending over
        boolean sendOver = false;
        // Control index
        int src_index = 0;
        // Packet info size
        int n_bytes = 64;
        // Destination initial position
        int dest_index= 0;
        // Remaining bytes to copy in the last packet
        int r_bytes = 64;
        
        while(!sendOver)
        {
            // If total bytes exceed Firmware size 
            if(src_index > (src.length-1))
            {
                 sendOver=true;
                 continue;
            } // No need for else
             
            // Calculate remaining bytes to be transfered
            r_bytes = src.length - src_index;
            
            // If there are more than 64 bytes to be transfered
            if(r_bytes < n_bytes)
            {
                System.arraycopy(src,src_index,dest,dest_index,r_bytes);
                src_index += r_bytes;
                sendCommandBytes(dest);
            }  
            else
            {
                System.arraycopy(src,src_index,dest,dest_index,n_bytes);
                src_index += n_bytes;
                sendCommandBytes(dest);
            }
            // Lacks reading response
        }
     
        return "OK";
    }
    
    /**
     * Send command to Machine
     * @param next Command
     * @return command length 
     */
    protected int sendCommand(String next) {

        next = clean(next);
        int cmdlen = 0;
        int i = 0;
        // skip empty commands.
        if (next.length() == 0) {
            return 0;
        }
        pipes = GetPipe(m_usbDevice);

        // do the actual send.
        String message = next + "\n";
        try {
            synchronized (m_usbDevice) {
                if (!pipes.isOpen()) {
                    openPipe(pipes);
                }
                pipes.getUsbPipeWrite().asyncSubmit(message.getBytes());
                cmdlen = next.length() + 1;
            }
        } catch (Exception ex) {
            //System.out.println("Error while sending command " + next + " : " + ex.getMessage());
        }
        return cmdlen;
    }
    
    /**
     * Send command to Machine
     * @param next Command
     * @return command length 
     */
    protected int sendCommandBytes(byte[] next) {

        int cmdlen = 0;
        int i = 0;

        pipes = GetPipe(m_usbDevice);

        try {
            synchronized (m_usbDevice) {
                if (!pipes.isOpen()) {
                    openPipe(pipes);
                }
                pipes.getUsbPipeWrite().asyncSubmit(next);
                cmdlen = next.length;
            }
        } catch (Exception ex) {
            //System.out.println("Error while sending command " + next + " : " + ex.getMessage());
        }
        return cmdlen;
    }

    /**
     * Reads response from Machine
     * @return response read
     */
    public String readResponse(){

        result = "";
        byte[] readBuffer = new byte[512];
        //System.out.println("a");
        int nBits = 0;
        try {
            synchronized (m_usbDevice) {
                nBits = pipes.getUsbPipeRead().syncSubmit(readBuffer);
            }
            //System.out.println(nBits);
        } catch (UsbException ex) {
            ///Logger.getLogger(UsbPassthroughDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            //Logger.getLogger(UsbPassthroughDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotOpenException ex) {
            //Logger.getLogger(UsbPassthroughDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            //Logger.getLogger(UsbPassthroughDriver.class.getName()).log(Level.SEVERE, null, ex);
        }

        // 0 is now an acceptable value; it merely means that we timed out
        // waiting for input
        if (nBits < 2) {
            // This signifies EOF. FIXME: How do we handle this?
            //System.out.println(USBConsole.class.getName() + ": EOF occured");
            //result = USBConsole.class.getName() + ": EOF occured";
            return "NO ACK";
        } else {
            try {
                result = new String(readBuffer, 0, nBits, "US-ASCII").trim();     
            } catch (UnsupportedEncodingException ex) {
                //Logger.getLogger(UsbPassthroughDriver.class.getName()).log(Level.SEVERE, null, ex);
            }
//            int index;
//            while ((index = result.indexOf('\n')) >= 0) {
//                String line = result.substring(0, index).trim();
//                result = result.substring(index + 1);
//                if (line.length() == 0) {
//                    continue;
//                }
//                //System.out.println("Linha =" + line);
//                result = line;
//            }

        }

        return result;
    }

    public boolean isFinished() {
        return isBufferEmpty();
    }

    public boolean isBufferEmpty() {

        return (bufferSize == 0);
    }

    public void dispose() {
        commands = null;
    }

    public void setInitialized(boolean status) {
        synchronized(isInitialized)
        {
                isInitialized.set(status);
                if (!status) { return; }
        }
    }
    
    private boolean isInitializedTop() {
        return isInitialized.get();
    }
    
    public boolean isInitialized() {
      if(isInitializedTop() && testPipes(pipes))
         return true;
      else 
         return false; 
   }
    
    public String clean(String str) {
        String clean = str;

        // trim whitespace
        clean = clean.trim();

        // remove spaces
        clean = clean.replaceAll(" ", "");

        return clean;
    }

    public synchronized void InitUsbDevice() {
        // Grab a lock
        m_usbLock.writeLock().lock();
        Usb4JavaDevice newDevice = null;
        try {
            UsbServices services = UsbHostManager.getUsbServices();
            UsbHub rootHub = services.getRootUsbHub();
            List devices = rootHub.getAttachedUsbDevices();

            for (int i = 0; i < devices.size(); i++) {

                //System.out.println(newDevice.getManufacturerString());

                if (devices.get(i).toString().contains("(ffff:014e)")) {
                    newDevice = (Usb4JavaDevice) devices.get(i);

                    if (newDevice.getManufacturerString().contains(m_manufacturer)
                            && newDevice.getProductString().contains(m_product)
                            && !newDevice.isUsbHub()) {
                        m_usbDevice = newDevice;
                        break;
                    }

                }

            }

            // select correct device
//            for (Object deviceObj : devices) {
//            for(int i = 0; i < devices.size();i++) {
//
//                newDevice = (Usb4JavaDevice) devices.get(i); 
//                Systeaam.out.println("i ="+ i + " :"+devices.get(i).toString());
//                if(newDevice.getManufacturerString()==null)
//                {
//                    continue;
//                } // No need for else
//                if(newDevice.getProductString()==null)
//                {
//                    continue;
//                } // No need for else 
////                
////                if (newDevice.getManufacturerString().trim().contains(m_manufacturer)
////                        && newDevice.getProductString().trim().contains(m_product)) {
////                    System.out.println(newDevice);
////                    Base.logger.log(Level.INFO, "Connecting to machine using usb device: " + newDevice.getProductString());
////                    m_usbDevice = newDevice;
////                    break;
////                } // No need for else
//
//            }

        } catch (Exception e) {
            //String message = "Error finding USB device: " + e.getMessage();
            String message = "Waiting ";
            if (!testPipes(pipes)) {
                message = "Error finding USB device. Check USB cable.";

            }

            //System.out.println(message);
//            Base.logger.severe(message);
//            setError(message);
        } finally {
            setInitialized(false);
            //System.out.println("FINALLY ="+newDevice.toString());
            m_usbDevice = newDevice;
            m_usbLock.writeLock().unlock();
        }

    }

    /**
     *
     * @return
     */
    protected UsbPipes GetPipe(UsbDevice device) {
        UsbConfiguration config = device.getActiveUsbConfiguration();
        //Assert.assertNotNull(config);

        if (pipes == null || !testPipes(pipes)) {
            pipes = new UsbPipes();
        } else {
            return pipes;
        }
        List interfaces = config.getUsbInterfaces();
        for (Object ifaceObj : interfaces) {
            UsbInterface iface = (UsbInterface) ifaceObj;
            if (iface.isClaimed() || !iface.isActive()) {
                continue;
            }
            
            List endpoints = iface.getUsbEndpoints();
            for (Object endpointObj : endpoints) {
                UsbEndpoint endpoint = (UsbEndpoint) endpointObj;

                if (endpoint.getType() != UsbConst.ENDPOINT_TYPE_BULK) {
                    continue;
                }

                if (endpoint.getDirection() == UsbConst.ENDPOINT_DIRECTION_OUT) {
                    this.pipes.setUsbPipeWrite(endpoint.getUsbPipe());
                }

                if (endpoint.getDirection() == UsbConst.ENDPOINT_DIRECTION_IN) {
                    this.pipes.setUsbPipeRead(endpoint.getUsbPipe());
                }
            }
            if (pipes.getUsbPipeRead() != null && pipes.getUsbPipeWrite() != null) {

                if (testPipes(pipes)) {
                    return pipes;
                } else {
                    continue;
                }

            } else {
                pipes.setUsbPipeWrite(null);
                pipes.setUsbPipeRead(null);
            }
        }

        return null;
    }

    protected boolean testPipes(UsbPipes pipes) {

        if (pipes.getUsbPipeWrite() == null || pipes.getUsbPipeRead() == null) {
            try {
                pipes.close();
                return false;
            } catch (UsbException ex) {
                return false;
            } catch (UsbNotActiveException ex) {
                return false;
            } catch (UsbNotOpenException ex) {
                return false;
            } catch (UsbDisconnectedException ex) {
                return false;
            }

        }

        UsbIrp usbIrp = pipes.getUsbPipeWrite().createUsbIrp();
        //usbIrp.setData(" M300 S0 P50\n ".getBytes());
        UsbIrp readUsbIrp = pipes.getUsbPipeRead().createUsbIrp();
        readUsbIrp.setAcceptShortPacket(true);
        try {
            if (!pipes.getUsbPipeRead().getUsbEndpoint().getUsbInterface().isClaimed()) {
                pipes.getUsbPipeRead().getUsbEndpoint().getUsbInterface().claim();
            }
            if (!pipes.isOpen()) {
                pipes.open();
            }
            pipes.getUsbPipeWrite().syncSubmit(usbIrp);
        } catch (UsbException ex) {
            //Logger.getLogger(UsbDriver.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (UsbNotActiveException ex) {
            setInitialized(false);
            return false;
        } catch (UsbNotOpenException ex) {
            setInitialized(false);
            return false;
        } catch (IllegalArgumentException ex) {
            //Logger.getLogger(UsbDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            setInitialized(false);
            return false;
        }
        return true;

    }

    /**
     *
     * @param pipe
     * @throws UsbClaimException
     * @throws UsbNotActiveException
     * @throws UsbDisconnectedException
     * @throws UsbException
     */
    protected void openPipe(UsbPipes pipes) {
        try {
            if (!pipes.getUsbEndpoint().getUsbInterface().isClaimed()) {
                pipes.getUsbEndpoint().getUsbInterface().claim();
            }
            if (!pipes.isOpen()) {
                pipes.open();
            }
            setInitialized(true);
        } catch (UsbClaimException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param pipe
     * @throws UsbClaimException
     * @throws UsbNotActiveException
     * @throws UsbDisconnectedException
     * @throws UsbException
     */
    protected void closePipe(UsbPipes pipes) {
        try {
            setInitialized(false);
            pipes.close();
            pipes.getUsbEndpoint().getUsbInterface().release();
        } catch (UsbException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotActiveException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbNotOpenException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UsbDisconnectedException ex) {
            Logger.getLogger(USBConsole.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
